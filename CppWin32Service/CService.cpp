#include "StdAfx.h"
#include "CService.hpp"

CService::CService(HANDLE stopEventHandle) : _stopEventHandle(stopEventHandle)
{
	//
}

CService::~CService()
{
	//
}

DWORD CService::Run()
{
	while (!IsStopRequested())
	{
		//
	}

	return NO_ERROR ;
}
