#include "StdAfx.h"
#include "CService.hpp"

int CWin32ServiceInternal::Install()
{
	wchar_t* pathToExecutable = new wchar_t[MAX_PATH];

	// �������� ���� � �������� ������, ����� ��������� ��� ��� ������
	if (!GetModuleFileNameW(NULL, pathToExecutable, MAX_PATH))
	{
		return -1; // todo
	}

	// ��������� ����������� � ���������� ���������� �������� �� ��������� ������ � ������� �� �������� ������
	SC_HANDLE scmHandle = OpenSCManagerW(NULL, NULL, SC_MANAGER_CONNECT | SC_MANAGER_CREATE_SERVICE);

	if (!scmHandle)
	{
		return -1; // todo
	}

	// ������ ������ � ������������, ����������� �� ��������� ������� ������
	SC_HANDLE serviceHandle = CreateServiceW(scmHandle, _serviceName, NULL, SERVICE_QUERY_STATUS,
	                                         SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_NORMAL,
	                                         pathToExecutable, NULL, NULL, L"", NULL, NULL);

	if (!serviceHandle)
	{
		return -1; // todo
	}

	CloseHandle(serviceHandle);
	CloseHandle(scmHandle);
	delete[] pathToExecutable;

	return 0;
}

int CWin32ServiceInternal::Uninstall()
{
	int result;

	// ��������� ����������� � ���������� ���������� �������� �� ��������� ������
	SC_HANDLE scmHandle = OpenSCManagerW(NULL, NULL, SC_MANAGER_CONNECT);

	if (!scmHandle)
	{
		return -1; // todo
	}

	// ��������� ������������ ������ � ������� �� ��������� � ��������
	SC_HANDLE serviceHandle = OpenServiceW(scmHandle, _serviceName, DELETE | SERVICE_STOP | SERVICE_QUERY_STATUS);

	if (!serviceHandle)
	{
		//if (GetLastError() == ERROR_SERVICE_DOES_NOT_EXIST)
		//{
		//	return 0; // ������ �� ����������
		//}

		return -1; // todo
	}

	result = DeleteService(serviceHandle); // ���� ������ �������, �� DeleteService() ��������� ��� ��������������

	CloseServiceHandle(serviceHandle);
	CloseServiceHandle(scmHandle);

	return (result) ? 0 : -1;
}

bool CWin32ServiceInternal::_IsStopRequested(HANDLE stopEventHandle)
{
	return (WaitForSingleObjectEx(stopEventHandle, 0, false) == WAIT_OBJECT_0);
}

namespace // ������������ ��� � ����������� C-style-��������� � ����������� �����������
{
	int ReportServiceStatus(DWORD dwCurrentState, DWORD dwWin32ExitCode, DWORD dwWaitHint, SERVICE_STATUS_HANDLE ssHandle, SERVICE_STATUS* const ss)
	{
		static DWORD dwCheckPoint = 1;

		ss->dwControlsAccepted = (dwCurrentState == SERVICE_START_PENDING) ? 0 : SERVICE_ACCEPT_STOP;
		ss->dwCheckPoint = ((dwCurrentState == SERVICE_RUNNING) || (dwCurrentState == SERVICE_STOPPED)) ? 0 : dwCheckPoint++;
		ss->dwCurrentState = dwCurrentState;
		ss->dwWin32ExitCode = dwWin32ExitCode;
		ss->dwWaitHint = dwWaitHint;

		return SetServiceStatus(ssHandle, ss) ? 0 : -1;
	}

	// ��������� ��� ����������� ���������� ����� ��������� �� ���������, �������� ���������, ����� ��������� CService::Run(), _ServiceMainInternal(),
	// _ServiceHandleControlInternal(), �� ���������� ������������ ���������� ����������. ������, ��������� �� ��������� ��������� ���� ������. 

	SERVICE_STATUS _gServiceStatus = {0};
	SERVICE_STATUS_HANDLE _gServiceStatusHandle = NULL;
	HANDLE _gStopEventHandle = NULL;

	DWORD __stdcall _ServiceHandleControlInternal(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
	{
		switch (dwControl)
		{
		case SERVICE_CONTROL_STOP:
			SetEvent(_gStopEventHandle);

			return NO_ERROR ;
		case SERVICE_CONTROL_INTERROGATE:
			return NO_ERROR ;
		}

		return ERROR_CALL_NOT_IMPLEMENTED;
	}

	//
	void __stdcall _ServiceMainInternal(DWORD dwNumServicesArgs, wchar_t** lpServiceArgVectors)
	{
		_gServiceStatusHandle = RegisterServiceCtrlHandlerExW(NULL, _ServiceHandleControlInternal, NULL);

		if (!_gServiceStatusHandle)
		{
			return; // todo: ��� ���������� ������? ReportServiceStatus() �������� ������..
		}

		ZeroMemory(&_gServiceStatus, sizeof(_gServiceStatus));

		_gServiceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
		_gServiceStatus.dwServiceSpecificExitCode = ERROR_SERVICE_SPECIFIC_ERROR;

		if (ReportServiceStatus(SERVICE_START_PENDING, NO_ERROR, 3000, _gServiceStatusHandle, &_gServiceStatus))
		{
			ReportServiceStatus(SERVICE_STOPPED, GetLastError(), 0, _gServiceStatusHandle, &_gServiceStatus);

			return;
		}

		_gStopEventHandle = CreateEventW(NULL, FALSE, FALSE, NULL);

		if (!_gStopEventHandle)
		{
			ReportServiceStatus(SERVICE_STOPPED, GetLastError(), 0, _gServiceStatusHandle, &_gServiceStatus);

			return;
		}

		void* serviceState = NULL;
		DWORD dwWin32ExitCode = ServiceInit(&serviceState, _gStopEventHandle);

		if (dwWin32ExitCode != NO_ERROR)
		{
			goto CLEANUP;
		}

		ReportServiceStatus(SERVICE_RUNNING, dwWin32ExitCode, 0, _gServiceStatusHandle, &_gServiceStatus);

		dwWin32ExitCode = ServiceMain(serviceState);

	CLEANUP:
		ReportServiceStatus(SERVICE_STOP_PENDING, dwWin32ExitCode, 0, _gServiceStatusHandle, &_gServiceStatus);

		ServiceCleanup(serviceState);
		CloseHandle(_gStopEventHandle);

		ReportServiceStatus(SERVICE_STOPPED, dwWin32ExitCode, 0, _gServiceStatusHandle, &_gServiceStatus);
	}
}

int CWin32ServiceInternal::Exec()
{
	SERVICE_TABLE_ENTRYW serviceTableEntry = {0};

	serviceTableEntry.lpServiceName = L""; // ��������� ������ ��������������� � SERVICE_WIN32_OWN_PROCESS, ��� ��� ����� �� ���������
	serviceTableEntry.lpServiceProc = _ServiceMainInternal;

	SERVICE_TABLE_ENTRYW dispatchTable[] = {
		serviceTableEntry,
		{NULL, NULL} // ������� ����� �������
	};

	return (!StartServiceCtrlDispatcherW(dispatchTable)) ? -1 : 0;
}
