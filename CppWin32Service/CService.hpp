#pragma once

#include "StdAfx.h"

// ������ ������� ������ ���������� ��������� ��� ������ ��� NO_ERROR
// ���������� ������������, ����� stopEventHandle ���������� ���-�� ������ state
extern DWORD ServiceInit(void**, HANDLE stopEventHandle);
extern DWORD ServiceMain(void*);
extern void ServiceCleanup(void*);

class CWin32ServiceInternal
{
	const wchar_t* _serviceName;
public:
	CWin32ServiceInternal(const wchar_t* serviceName) : _serviceName(serviceName)
	{
	};

	int Install();
	int Uninstall();
	int Exec();

	static bool _IsStopRequested(HANDLE stopEventHandle);
};

class CService
{
	HANDLE _stopEventHandle;
public:
	CService(HANDLE stopEventHandle);
	~CService();

	bool IsStopRequested()
	{
		return CWin32ServiceInternal::_IsStopRequested(_stopEventHandle);
	}

	DWORD Run();
};
