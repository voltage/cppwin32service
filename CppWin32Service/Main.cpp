// Main.cpp : Defines the entry point for the console application.
//

#include "StdAfx.h"
#include "CService.hpp"

int wmain(int argc, const wchar_t** argv)
{
	CWin32ServiceInternal win32ServiceInternal(L"CppWin32Service"); // todo: �������� ���

	if (argc != 2)
	{
		// ������������, ��� ����� ��� ������ ����������� ���������� ��������
		return win32ServiceInternal.Exec();
	}

	std::ios::sync_with_stdio(false); // ��������� �����-������

	if (!wcscmp(argv[1], L"install"))
	{
		std::wcout << L"install.... " << (!(win32ServiceInternal.Install()) ? L"ok" : L"fail") << std::endl;
	}
	else if (!wcscmp(argv[1], L"uninstall"))
	{
		std::wcout << L"uninstall.... " << (!(win32ServiceInternal.Uninstall()) ? L"ok" : L"fail") << std::endl;
	}

	return 0;
}

DWORD ServiceInit(void** pState, HANDLE stopEventHandle)
{
	try
	{
		*pState = new CService(stopEventHandle);
	}
	catch (const std::bad_alloc& e)
	{
		(void)(e); // C4101

		return ERROR_NOT_ENOUGH_MEMORY ;
	}

	return NO_ERROR ;
}

DWORD ServiceMain(void* state)
{
	return static_cast<CService*>(state)->Run();
}

void ServiceCleanup(void* state)
{
	delete state;
}
